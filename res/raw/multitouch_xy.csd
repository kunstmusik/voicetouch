<CsoundSynthesizer>
<CsOptions>
-o dac -+rtaudio=null -d   -b512
</CsOptions>
<CsInstruments>
nchnls=2
0dbfs=1
ksmps=64
sr = 44100

ga1 init 0

	opcode vowel2,a,aki

asig,kmorf,imode xin

imorf	ftgentmp 0, 0, 16, 10, 1; must be 16 elements long because vowels are in tables of length 16
ifenv	ftgentmp 0, 0, 4096, 19, .5, .5, 270, .5
ivib	ftgentmp 0, 0, 4096, 10, 1

itables chnget "vowel2.tables"


if(itables == 0) then

ibass_a		ftgen 0, 0, 16, -2, 600, 1040, 2250, 2450, 2750, 0,  -7,  -9,  -9, -20, 60, 70, 110, 120, 130
ibass_e		ftgen 0, 0, 16, -2, 400, 1620, 2400, 2800, 3100, 0, -12,  -9, -12, -18, 40, 80, 100, 120, 120
ibass_i		ftgen 0, 0, 16, -2, 350, 1700, 2700, 3700, 4950, 0, -20, -30, -22, -28, 60, 90, 100, 120, 120
ibass_o		ftgen 0, 0, 16, -2, 450, 800,  2830, 3500, 4950, 0, -11, -21, -20, -40, 40, 80, 100, 120, 120
ibass_u		ftgen 0, 0, 16, -2, 325, 700,  2530, 3500, 4950, 0, -20, -32, -28, -36, 40, 80, 100, 120, 120

itenor_a	ftgen 0, 0, 16, -2, 650, 1080, 2650, 2900, 3250, 0,  -6,  -7,  -8, -22, 80, 90, 120, 130, 140	
itenor_e	ftgen 0, 0, 16, -2, 400, 1700, 2600, 3200, 3580, 0, -14, -12, -14, -20, 70, 80, 100, 120, 120	
itenor_i	ftgen 0, 0, 16, -2, 290, 1870, 2800, 3250, 3540, 0, -15, -18, -20, -30, 40, 90, 100, 120, 120	
itenor_o	ftgen 0, 0, 16, -2, 400,  800, 2600, 2800, 3000, 0, -10, -12, -12, -26, 70, 80, 100, 130, 135	
itenor_u	ftgen 0, 0, 16, -2, 350,  600, 2700, 2900, 3300, 0, -20, -17, -14, -26, 40, 60, 100, 120, 120

icountertenor_a	ftgen 990, 0, 16, -2, 660, 1120, 2750, 3000, 3350, 0,  -6, -23, -24, -38, 80, 90, 120, 130, 140	
icountertenor_e	ftgen 991, 0, 16, -2, 440, 1800, 2700, 3000, 3300, 0, -14, -18, -20, -20, 70, 80, 100, 120, 120	
icountertenor_i	ftgen 992, 0, 16, -2, 270, 1850, 2900, 3350, 3590, 0, -24, -24, -36, -36, 40, 90, 100, 120, 120	
icountertenor_o	ftgen 993, 0, 16, -2, 430,  820, 2700, 3000, 3300, 0, -10, -26, -22, -34, 40, 80, 100, 120, 120	
icountertenor_u	ftgen 994, 0, 16, -2, 370,  630, 2750, 3000, 3400, 0, -20, -23, -30, -34, 40, 60, 100, 120, 120

ialto_a		ftgen 0, 0, 16, -2, 800, 1150, 2800, 3500, 4950, 0,  -4, -20, -36, -60, 80,  90, 120, 130, 140
ialto_e		ftgen 0, 0, 16, -2, 400, 1600, 2700, 3300, 4950, 0, -24, -30, -35, -60, 60,  80, 120, 150, 200
ialto_i		ftgen 0, 0, 16, -2, 350, 1700, 2700, 3700, 4950, 0, -20, -30, -36, -60, 50, 100, 120, 150, 200
ialto_o		ftgen 0, 0, 16, -2, 450, 800,  2830, 3500, 4950, 0,  -9, -16, -28, -55, 70,  80, 100, 130, 135
ialto_u		ftgen 0, 0, 16, -2, 325, 700,  2530, 3500, 4950, 0, -12, -30, -40, -64, 50,  60, 170, 180, 200

isoprano_a	ftgen 0, 0, 16, -2, 800, 1150, 2900, 3900, 4950, 0,  -6, -32, -20, -50, 80,  90, 120, 130, 140	
isoprano_e	ftgen 0, 0, 16, -2, 350, 2000, 2800, 3600, 4950, 0, -20, -15, -40, -56, 60, 100, 120, 150, 200	
isoprano_i	ftgen 0, 0, 16, -2, 270, 2140, 2950, 3900, 4950, 0, -12, -26, -26, -44, 60,  90, 100, 120, 120	
isoprano_o	ftgen 0, 0, 16, -2, 450,  800, 2830, 3800, 4950, 0, -11, -22, -22, -50, 40,  80, 100, 120, 120	
isoprano_u	ftgen 0, 0, 16, -2, 325,  700, 2700, 3800, 4950, 0, -16, -35, -40, -60, 50,  60, 170, 180, 200

itables		ftgen 0, 0, -25, -2, ibass_a, ibass_e, ibass_i, ibass_o, ibass_u, \
				itenor_a, itenor_e, itenor_i, itenor_o, itenor_u, \
				icountertenor_a, icountertenor_e, icountertenor_i, icountertenor_o, icountertenor_u, \
				ialto_a, ialto_e, ialto_i, ialto_o, ialto_u, \
				isoprano_a, isoprano_e, isoprano_i, isoprano_o, isoprano_u

chnset itables, "vowel2.tables"

endif

istartIndex = imode * 5
ia table istartIndex, itables
ie table istartIndex + 1, itables
ii table istartIndex + 2, itables
io table istartIndex + 3, itables
iu table istartIndex + 4, itables

index	ftgentmp 0, 0, 16, -2, ia, ie, ii, ia, io, iu, ie, io, ii, iu, ia, io, ia, ia, ia, ia, ia

	ftmorf	kmorf, index, imorf

kfx	=	0
kform1	table	kfx,   imorf
kform2	table	kfx+1, imorf
kform3	table	kfx+2, imorf
kform4	table	kfx+3, imorf
kform5	table	kfx+4, imorf
kamp1	table	kfx+5, imorf
kamp2	table	kfx+6, imorf
kamp3	table	kfx+7, imorf
kamp4	table	kfx+8, imorf
kamp5	table	kfx+9, imorf
kbw1	table	kfx+10,imorf
kbw2	table	kfx+11,imorf
kbw3	table	kfx+12, imorf
kbw4	table	kfx+13, imorf
kbw5	table	kfx+14, imorf

;iolaps	=	200

a1 butbp asig*db(kamp1), kform1, kbw1
a2 butbp asig*db(kamp2), kform2, kbw2
a3 butbp asig*db(kamp3), kform3, kbw3
a4 butbp asig*db(kamp4), kform4, kbw4
a5 butbp asig*db(kamp5), kform5, kbw5

asig	sum a1, a2, a3, a4, a5



	xout	asig


	endop
	
	opcode chorus,aa,aak

ain1, ain2, kwet xin

setksmps 1

; CHORUS

isin		ftgentmp	0, 0, 65536, 10, 1

ilevl		=		0.3		; Output level
idelay		=		0.01		; Delay in ms
idpth		=		0.002		; Depth in ms
imax		=		0.25		; Maximum LFO rate
imin		=		0.5		; Minimum LFO rate
iwave		=		isin		; LFO waveform

ain             =               ain1 + ain2 * .5
ain             =               ain * ilevl

i01             =               rnd(imax)
i02             =               rnd(imax)
i03             =               rnd(imax)
i04             =               rnd(imax)
i05             =               rnd(imax)
i06             =               rnd(imax)
i07             =               rnd(imax)
i08             =               rnd(imax)
i09             =               rnd(imax)
i10             =               rnd(imax)
i11             =               rnd(imax)
i12             =               rnd(imax)

alfo01          oscil           idpth, i01 + imin, iwave
alfo02          oscil           idpth, i02 + imin, iwave, .08
alfo03          oscil           idpth, i03 + imin, iwave, .17
alfo04          oscil           idpth, i04 + imin, iwave, .25
alfo05          oscil           idpth, i05 + imin, iwave, .33
alfo06          oscil           idpth, i06 + imin, iwave, .42
alfo07          oscil           idpth, i07 + imin, iwave, .50
alfo08          oscil           idpth, i08 + imin, iwave, .58
alfo09          oscil           idpth, i09 + imin, iwave, .67
alfo10          oscil           idpth, i10 + imin, iwave, .75
alfo11          oscil           idpth, i11 + imin, iwave, .83
alfo12          oscil           idpth, i12 + imin, iwave, .92

atemp           delayr          idelay + idpth +.1
a01             deltapi         idelay + alfo01
a02             deltapi         idelay + alfo02
a03             deltapi         idelay + alfo03
a04             deltapi         idelay + alfo04
a05             deltapi         idelay + alfo05
a06             deltapi         idelay + alfo06
a07             deltapi         idelay + alfo07
a08             deltapi         idelay + alfo08
a09             deltapi         idelay + alfo09
a10             deltapi         idelay + alfo10
a11             deltapi         idelay + alfo11
a12             deltapi         idelay + alfo12
                delayw          ain

achorusl        sum		a01, a02, a03, a04, a05, a06
achorusr        sum             a07, a08, a09, a10, a11, a12

aout1		=		ain1 * (1-kwet) + achorusl * kwet
aout2		=		ain2 * (1-kwet) + achorusr * kwet

xout	aout1, aout2

	endop

instr 1
itie tival
i_instanceNum = p4
S_xName sprintf "touch.%d.x", i_instanceNum
S_yName sprintf "touch.%d.y", i_instanceNum

kx chnget S_xName
ky chnget S_yName

ky limit ky, 0, 1
kx limit kx, 0, 1

kenv linsegr 0, .01, 1, .1, 1, .25, 0
;a1 vco2 ky^2 * .5 * kenv, 60 + (log(1 - kx) * 3000), 0
a1 vco2 ky^2 * kenv, 200 + (log(1 - kx) * 1500), 0

ga1 = ga1 + a1

endin

instr 2

ibp_max_width = 200

kmorf chnget "morph"
kvoice_type chnget "voiceType"
kchorus chnget "chorus"
kreverb chnget "reverb"
kvolume chnget "volume"

reset:

itype = i(kvoice_type)

if (kvoice_type != itype) then
	reinit reset
endif

kmorfsmooth port kmorf, .025

aout vowel2 ga1, kmorfsmooth, itype 

rireturn

kvol = ((kvolume^4) * 54) - 30  ;within -30 and +12 db

aout = aout * ampdb(kvol)

aL, aR chorus aout, aout, kchorus
aL, aR reverbsc aL, aR, kreverb, 5000

outs aL, aR

ga1 = 0

endin


</CsInstruments>
<CsScore>
f1 0 16384 10 1

i2 0 360000
 
</CsScore>
</CsoundSynthesizer>
