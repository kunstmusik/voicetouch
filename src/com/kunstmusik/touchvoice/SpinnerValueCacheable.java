package com.kunstmusik.touchvoice;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

import com.csounds.CsoundObj;
import com.csounds.valueCacheable.CsoundValueCacheable;

import csnd.CsoundMYFLTArray;

public class SpinnerValueCacheable implements CsoundValueCacheable {

	private CsoundObj csoundObj;
	private int cachedPosition;
	private Spinner spinner;
	private CsoundMYFLTArray channel;
	private String channelName;
	boolean dirty = true;
	OnItemSelectedListener listener;
	
	public SpinnerValueCacheable(Spinner spinner, String channelName) {
		this.spinner = spinner;
		this.channelName = channelName;
		
		listener = new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				cachedPosition = pos;
				dirty = true;
			}

			public void onNothingSelected(AdapterView<?> arg0) {
			}
		};
		
		spinner.setOnItemSelectedListener(listener);
	}
	
	public void setup(CsoundObj csoundObj) {
		this.csoundObj = csoundObj;
		this.cachedPosition = spinner.getSelectedItemPosition();
		this.channel = csoundObj.getInputChannelPtr(channelName);
		this.channel.SetValue(0, cachedPosition);
	}

	public void updateValuesToCsound() {

		if(dirty) {
			this.channel.SetValue(0, cachedPosition);
			dirty = false;
		}
		
	}

	public void updateValuesFromCsound() {
	}

	public void cleanup() {
		spinner.setOnItemSelectedListener(null);
		channel.Clear();
		channel = null;
	}

}
