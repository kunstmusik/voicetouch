package com.kunstmusik.touchvoice;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.csounds.CsoundObj;
import com.csounds.valueCacheable.CsoundValueCacheable;

import csnd.CsoundMYFLTArray;

public class MultiTouchView extends View implements CsoundValueCacheable {
	
	private static final int TOUCH_MAX = 10;
	
	int touchIds[] = new int[TOUCH_MAX];
	float touchX[] = new float[TOUCH_MAX];
	float touchY[] = new float[TOUCH_MAX];
	CsoundMYFLTArray touchXPtr[] = new CsoundMYFLTArray[TOUCH_MAX];
	CsoundMYFLTArray touchYPtr[] = new CsoundMYFLTArray[TOUCH_MAX];

	private CsoundObj csoundObj = null;

	private String scoreDownString = null;

	private String scoreUpString = null;
	
	private String touchXString = null;
	private String touchYString = null;

	public MultiTouchView(Context context) {
		super(context);
	}
	
	public MultiTouchView(Context context, AttributeSet attr) {
		super(context, attr);
	}
	
	public MultiTouchView(Context context, AttributeSet attr, int style) {
		super(context, attr, style);
	}
	
	public void initTouch(CsoundObj csObj, String scoreDownStr, String scoreUpStr,
			String touchXStr, String touchYStr) {
		this.csoundObj = csObj;
		this.scoreDownString = scoreDownStr;
		this.scoreUpString = scoreUpStr;
		this.touchXString = touchXStr;
		this.touchYString = touchYStr;
		
		for (int i = 0; i < touchIds.length; i++) {
			touchIds[i] = -1;
			touchX[i] = -1;
			touchY[i] = -1;
		}

		setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				final int action = event.getAction() & MotionEvent.ACTION_MASK;
				switch (action) {
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_POINTER_DOWN:

					for (int i = 0; i < event.getPointerCount(); i++) {
						int pointerId = event.getPointerId(i);
						int id = getTouchId(pointerId);

						if (id == -1) {

							id = getTouchIdAssignment();

							if (id != -1) {
								touchIds[id] = pointerId;
								touchX[id] = event.getX(i)
										/ getWidth();
								touchY[id] = 1 - (event.getY(i) / 
										getHeight());

								if (touchXPtr[id] != null) {
									touchXPtr[id].SetValue(0, touchX[id]);
									touchYPtr[id].SetValue(0, touchY[id]);

									if(scoreDownString != null) {
										csoundObj.sendScore(String.format(
												scoreDownString, id, id));
									}
								}
							}
						}

					}

					break;
				case MotionEvent.ACTION_MOVE:

					for (int i = 0; i < event.getPointerCount(); i++) {
						int pointerId = event.getPointerId(i);
						int id = getTouchId(pointerId);

						if (id != -1) {
							touchX[id] = event.getX(i)
									/ getWidth();
							touchY[id] = 1 - (event.getY(i) / getHeight());
						}

					}
					break;
				case MotionEvent.ACTION_POINTER_UP:
				case MotionEvent.ACTION_UP: {
					int activePointerIndex = event.getActionIndex();
					int pointerId = event.getPointerId(activePointerIndex);

					int id = getTouchId(pointerId);
					if (id != -1) {
						touchIds[id] = -1;
						if(scoreUpString != null) {
							csoundObj.sendScore(String.format(
									scoreUpString, id,
									id));
						}
					}

				}
					break;
				}
				return true;
			}

		});
	}

	
	protected int getTouchIdAssignment() {
		for (int i = 0; i < touchIds.length; i++) {
			if (touchIds[i] == -1) {
				return i;
			}
		}
		return -1;
	}

	protected int getTouchId(int touchId) {
		for (int i = 0; i < touchIds.length; i++) {
			if (touchIds[i] == touchId) {
				return i;
			}
		}
		return -1;
	}



	// VALUE CACHEABLE

	public void setup(CsoundObj csoundObj) {
		for (int i = 0; i < touchIds.length; i++) {
			touchXPtr[i] = csoundObj.getInputChannelPtr(String.format(
					touchXString, i));
			touchYPtr[i] = csoundObj.getInputChannelPtr(String.format(
					touchYString, i));
		}
	}

	public void updateValuesToCsound() {
		for (int i = 0; i < touchX.length; i++) {
			touchXPtr[i].SetValue(0, touchX[i]);
			touchYPtr[i].SetValue(0, touchY[i]);
		}

	}

	public void updateValuesFromCsound() {
	}

	public void cleanup() {
		for (int i = 0; i < touchIds.length; i++) {
			touchXPtr[i].Clear();
			touchXPtr[i] = null;
			touchYPtr[i].Clear();
			touchYPtr[i] = null;
		}
	}
}
