package com.kunstmusik.touchvoice;

import java.io.File;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.csounds.CsoundObj;

import csnd.CsoundMYFLTArray;

public class MainActivity extends BaseCsoundActivity {

	SeekBar seekBar;
	SeekBar chorusBar;
	SeekBar reverbBar;
	SeekBar volumeBar;
	MultiTouchView view2;
	Spinner voiceType;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		seekBar = (SeekBar) findViewById(R.id.seekBar1);
		chorusBar = (SeekBar) findViewById(R.id.chorusBar);
		reverbBar = (SeekBar) findViewById(R.id.reverbBar);
		volumeBar = (SeekBar) findViewById(R.id.volumeBar);
		view2 = (MultiTouchView) findViewById(R.id.view2);
		voiceType = (Spinner) findViewById(R.id.spinner1);
		
		ArrayAdapter<CharSequence> adapter = ArrayAdapter
                .createFromResource(this, R.array.voice_types,
                                android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		voiceType.setAdapter(adapter);
		
		csoundObj.addValueCacheable(new SpinnerValueCacheable(voiceType, "voiceType"));
		
		chorusBar.setProgress(50);
		reverbBar.setProgress(72);
		volumeBar.setProgress(86);
		
		csoundObj.addSlider(seekBar, "morph", 0.0, 5.0);
		csoundObj.addSlider(chorusBar, "chorus", 0.0, 1.0);
		csoundObj.addSlider(reverbBar, "reverb", 0.0, 1.0);
		csoundObj.addSlider(volumeBar, "volume", 0, 1.0);
		
		view2.initTouch(csoundObj, 
				"i1.%d 0 -2 %d", 
				"i-1.%d 0 0 %d",
				"touch.%d.x", 
				"touch.%d.y");

		
		String csd = getResourceFileAsString(R.raw.multitouch_xy);
		File f = createTempFile(csd);

		csoundObj.addValueCacheable(view2);
		csoundObj.setMessageLoggingEnabled(true);
		csoundObj.startCsound(f);
	}

	public void csoundObjComplete(CsoundObj csoundObj) {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
